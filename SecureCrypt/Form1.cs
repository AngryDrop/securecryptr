﻿using Anubis;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureCrypt
{
    public partial class Form1 : Form
    {
        RegistryKey reg;
        string REG_KEY_BASE = @"Software\SecureCryptr\CryptedFiles";

        public Form1()
        {
            InitializeComponent();
            if (Registry.CurrentUser.OpenSubKey(REG_KEY_BASE) == null)
            {
                Registry.CurrentUser.CreateSubKey(REG_KEY_BASE);
                consoleTextBox.AppendText("Created New Registry Entry\n");
            }
            reg = Registry.CurrentUser.OpenSubKey(REG_KEY_BASE, true);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            consoleTextBox.AppendText("SecureCryptr (Alpha V1.0) by Landon Hammond\n");

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();
            folderBrowserDialog.ShowDialog();

            if (folderBrowserDialog.SelectedPath != null && folderBrowserDialog.SelectedPath != "")
            {
                textBoxFolder.Text = folderBrowserDialog.SelectedPath;
                string[] allFiles = @reg.GetValueNames();
                bool isEncrypted = false;
                foreach (string file in allFiles)
                {
                    if (Path.GetDirectoryName(file).Contains(textBoxFolder.Text))
                    {
                        isEncrypted = true;
                        break;
                    }
                }

                if (isEncrypted)
                {
                    btnEncrypt.Enabled = false;
                    btnDecrypt.Enabled = true;
                }
                else
                {
                    btnEncrypt.Enabled = true;
                    btnDecrypt.Enabled = false;
                }
                consoleTextBox.AppendText("Selected Folder: " + folderBrowserDialog.SelectedPath + "\n");
                ReportReset();
            }
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
           
           bgwEncrypt.RunWorkerAsync(textBoxFolder.Text);
           btnEncrypt.Enabled = false;
           btnDecrypt.Enabled = false;
        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            bgwDecrypt.RunWorkerAsync(textBoxFolder.Text);
            btnEncrypt.Enabled = false;
            btnDecrypt.Enabled = false;
        }

        private void bgwEncrypt_DoWork(object sender, DoWorkEventArgs e)
        {
            ReportFileActionLabel("Files Encrypted:");
            Stopwatch timer = new Stopwatch();
            timer.Start();
            string rootFolder = (String)e.Argument;
            string[] files = Directory.GetFiles(rootFolder, "*", SearchOption.AllDirectories);
            //update count of files (GUI) and set progress bar max to numfiles
            ReportMaxProgVal(files.Length);
            int count = 0;
            foreach(string file in files){
                //update current file label (GUI) && Log to Console Window
                ReportCurrentProgVal(++count);
                ReportCurrentFileLabel(file);
                ReportConsoleTextBox("Encrypted " + file);
                ReportTimeLabel(timer.Elapsed.ToString());
                //generate key
                try
                {
                    string key = Encryption.GenerateRandomKey();

                    Encryption.Encrypt(@file, @file + ".Anubis", key);
                    
                   
                    //set registry key
                    reg.SetValue(file + ".Anubis", key);

                    
                }
                catch (Exception ex)
                {

                }
            }
            ReportBtnDecryptEnabled(true);
            
        }

        private void bgwDecrypt_DoWork(object sender, DoWorkEventArgs e)
        {
            ReportFileActionLabel("Files Decrypted:");
            Stopwatch timer = new Stopwatch();
            timer.Start();
            string rootFolder = (String)e.Argument;
            string[] allFiles = reg.GetValueNames();
            List<string> fileList = new List<string>();
            
            int count = 0;
            foreach (string file in allFiles)
            {
                if (Path.GetDirectoryName(file).Contains(rootFolder))
                {
                    fileList.Add(file);
                }
            }

            ReportMaxProgVal(fileList.Count);
            foreach (string file in fileList)
            {
                string file_decrypted = file.Remove(file.Length - 6);

                //update current file label (GUI) && Log to Console Window
                ReportCurrentProgVal(++count);
                ReportCurrentFileLabel(file);
                ReportConsoleTextBox("Decrypted " + file);
                ReportTimeLabel(timer.Elapsed.ToString());

                string key = (string)reg.GetValue(file);

                Encryption.Decrypt(@file, @file_decrypted, key);
               
                
                reg.DeleteValue(file);
            }
            ReportBtnEncryptEnabled(true);
        }

        //____________DELAGATES________________
        private void ReportConsoleTextBox(string str)
        {

            if (!lblCurrentFile.InvokeRequired)
            {

                consoleTextBox.AppendText(str+"\n");
            }
            else
            {
                SetConsoleCallback d = new SetConsoleCallback(ReportConsoleTextBox);
                this.Invoke(d, new object[] { str });
            }

        }

        private void ReportCurrentFileLabel(string ix)
        {

            if (!lblCurrentFile.InvokeRequired)
            {
                
                lblCurrentFile.Text = Path.GetFileName(ix);
            }
            else
            {
                SetCurrentFileCallback d = new SetCurrentFileCallback(ReportCurrentFileLabel);
                this.Invoke(d, new object[] { ix });
            }

        }

        private void ReportTimeLabel(string str)
        {

            if (!lblCurrentFile.InvokeRequired)
            {

                lblTime.Text = str;
            }
            else
            {
                SetTimeCallback d = new SetTimeCallback(ReportTimeLabel);
                this.Invoke(d, new object[] { str });
            }

        }

        private void ReportFileActionLabel(string str)
        {

            if (!lblCurrentFile.InvokeRequired)
            {
               lblFilesAction.Text = str;
            }
            else
            {
                SetFileActionCallback d = new SetFileActionCallback(ReportFileActionLabel);
                this.Invoke(d, new object[] { str });
            }

        }

        private void ReportMaxProgVal(int max)
        {
            if (!progBar.InvokeRequired)
            {
                progBar.Maximum = max;
                lblEncCount.Text = "(" + progBar.Value + "/" + progBar.Maximum + ")";
            }
            else
            {
                SetMaxProgValCallback d = new SetMaxProgValCallback(ReportMaxProgVal);
                this.Invoke(d, new object[] { max });
            }
        }

        private void ReportCurrentProgVal(int cur)
        {
            if (!progBar.InvokeRequired)
            {
                progBar.Value = cur;
                lblEncCount.Text = "(" + progBar.Value + "/" + progBar.Maximum + ")";
            }
            else
            {
                SetCurrentProgValCallback d = new SetCurrentProgValCallback(ReportCurrentProgVal);
                this.Invoke(d, new object[] { cur });
            }
        }

        private void ReportBtnEncryptEnabled(bool enabledBool)
        {
            if (!btnEncrypt.InvokeRequired)
            {
                btnEncrypt.Enabled = enabledBool;
            }
            else
            {
                SetBtnEncryptEnabledCallback d = new SetBtnEncryptEnabledCallback(ReportBtnEncryptEnabled);
                this.Invoke(d, new object[] { enabledBool });
            }
        }

        private void ReportBtnDecryptEnabled(bool enabledBool)
        {
            if (!btnDecrypt.InvokeRequired)
            {
                btnDecrypt.Enabled = enabledBool;
            }
            else
            {
                SetBtnEncryptEnabledCallback d = new SetBtnEncryptEnabledCallback(ReportBtnDecryptEnabled);
                this.Invoke(d, new object[] { enabledBool });
            }
        }

        private void ReportReset()
        {
            if (!progBar.InvokeRequired)
            {
                progBar.Value = 0;
                progBar.Maximum = 0;
                lblEncCount.Text = "(0/0)";
                lblCurrentFile.Text = "None";
            }
            else
            {
                SetResetCallback d = new SetResetCallback(ReportReset);
                this.Invoke(d, new object[] { });
            }
        }

        
        delegate void SetConsoleCallback(string str);
        delegate void SetCurrentFileCallback(string file);
        delegate void SetTimeCallback(string str);
        delegate void SetFileActionCallback(string str);
        delegate void SetMaxProgValCallback(int max);
        delegate void SetCurrentProgValCallback(int cur);
        delegate void SetBtnEncryptEnabledCallback(bool enabledBool);
        delegate void SetBtnDecryptEnabledCallback(bool enabledBool);
        delegate void SetResetCallback();

        //_______MENU STRIP__________

        private void serverLoginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Login l = new Login();
            l.Show();
        }

        private void registerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://service.4streamline.com/");
        }
    }
}
